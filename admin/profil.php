 <?php
                session_start();
                include 'koneksi.php';
                $id_admin = $_SESSION['id_admin'];
                $query_admin_putri=mysqli_query($koneksi, "SELECT * FROM admin WHERE $id_admin='$id_admin'");
                $admin=mysqli_fetch_array($query_admin_putri);
                $nama_admin=$admin['nama_admin'];
                ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Putri PLN</title>
    <link href="style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><span>P P L N</span></a>
                    </div>
                    <div class="clearfix"></div>
                <br />
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a href="informasi.php"><i class="fa fa-user"></i> Profile Account </a>
                                </li>
                                <li><a href="saldo.php"><i class="fa fa-dollar"></i> Saldo Information </span></a>
                                </li>
                                <li><a href="penggunaan.php"><i class="fa fa-edit"></i> Use </span></a>
                                </li>
                                <li><a href="tagihan.php"><i class="fa fa-desktop"></i> Bill </a>
                                </li>
                                <li><a href="riwayatpembelian.php"><i class="fa fa-table"></i> Transaction History </a>
                                </li>
                            </ul>
                        </div>
                        </div>
                    <!-- /sidebar menu -->
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                    </nav>
                    <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <div class=" fa fa-list"></div>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="bantuan.php">Help</a>
                                    </li>
                                    <li><a href="logout.php">Log Out</a>
                                    </li>
                                    <li><a href="bahasaIndonesia/informasi.php">Bahasa Indonesia</a>
                                    </li>
                                </ul>
                            </li>
                                <li role="presentation" class="dropdown">
                         </li>
                     </ul>
                </div>
            </div>

            <div class="right_col" role="main">

                <!-- top tiles -->
               
                <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Profile</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <div class="table-responsive">
                                  <table class="table">
                                    <tr>
                                        <td>Username</td>
                                        <td><?=$pelanggan['username'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Admin</td>
                                        <td><?=$pelanggan['nama_admin'];?></td>
                                    </tr>
                                    <tr>
                                        <td>ID Level</td>
                                        <td><?=$pelanggan['id_level'];?></td>
                                    </tr>
                                    </table>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                <!-- /top tiles -->
                
            </div>
        </div>

    </div>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>
