<?php
                                        	include "koneksi.php"
                                        	?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Putri PLN</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><span>P P L N</span></a>
                    </div>
                    <div class="clearfix"></div>
                <br />
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a href="profil.php"><i class="fa fa-user"></i> Profil Admin </a>
                                </li>
                                <li><a href="riwayattransaksi.php"><i class="fa fa-edit"></i>Riwayat Transaksi</span></a>
                                </li>
                                <li><a href="verifikasi.php"><i class="fa fa-desktop"></i>Verifikasi dan Validasi</a>
                                </li>
                                <li><a href="datapelanggan.php"><i class="fa fa-desktop"></i>Data Pelanggan</a>
                                </li>
                                <li><a href="laporan.php"><i class="fa fa-table"></i>Generate Laporan</a>
                                </li>
                            </ul>
                        </div>
                        </div>
                    <!-- /sidebar menu -->
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                    </nav>
                    <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <div class=" fa fa-list"></div>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="bantuan.php">Help</a>
                                    </li>
                                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                                <li role="presentation" class="dropdown">
                         </li>
                     </ul>
                </div>
            </div>
            <div class="right_col" role="main">
                <!-- top tiles -->
                 <div class="content">
                        <div class="col-md-12" >
                                <div class="x_title">
                                    <h2>Riwayat Transaksi</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                 <table id="example" class="table table-striped table-bordered responsive">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Tanggal</th>
                                                <th>Bulan</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                <!-- /top tiles -->
                
            </div>
        </div>

    </div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#example').DataTable();});
            </script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>
