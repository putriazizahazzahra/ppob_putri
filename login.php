<!DOCTYPE html>
<?php
include 'koneksi.php';
?>
<html>
<head>
  <title>PPLN</title>
  <link rel="stylesheet" href="style.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<style type="text/css">
  body{
  font-family: sans-serif;
  background: #f9da3a;
}

h1{
  text-align: center;
  /*ketebalan font*/
  font-weight: 300;
}

.tulisan_login{
  text-align: center;
  /*membuat semua huruf menjadi kapital*/
  text-transform: uppercase;
}

.kotak_login{
  width: 350px;
  background: white;
  /*meletakkan form ke tengah*/
  margin: 80px auto;
  padding: 30px 20px;
  
}

label{
  font-size: 11pt;
}

.form_login{
  /*membuat lebar form penuh*/
  box-sizing : border-box;
  width: 100%;
  padding: 10px;
  font-size: 11pt;
  margin-bottom: 20px;
}

.tombol_login{
  background: #2aa7e2;
  color: white;
  font-size: 11pt;
  width: 100%;
  border: none;
  border-radius: 3px;
  padding: 10px 20px;
}

.link{
  color: #232323;
  text-decoration: none;
  font-size: 10pt;
}

.alert{
  background: #e44e4e;
  color: white;
  padding: 10px;
  text-align: center;
  border:1px solid #b32929;
}
</style>
<body>
  <?php 
  if(isset($_GET['pesan'])){
    if($_GET['pesan']=="gagal"){
      echo "<div class='alert'>Username dan Password tidak sesuai !</div>";
    }
  }
  ?>
  <div class="kotak_login">
    <p class="tulisan_login">LOGIN</p>
 
    <form action="proseslogin.php" method="post">
      <label>Username</label>
      <input type="text" name="username" autocomplete="off" class="form_login" placeholder="Enter Username" required="required">
 
      <label>Password</label>
      <input type="password" name="password" autocomplete="off" class="form_login" placeholder="Enter Password" required="required">
 
      <input type="submit" class="tombol_login" name="login" value="LOGIN">
 
      <br/>
      <br/>
        <a href="registrasi.php">Belum mempunyai akun? Klik disini untuk mendaftar</a>     
    </form>
    
  </div>
 
 
</body>
</html>