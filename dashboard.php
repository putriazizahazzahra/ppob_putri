 <?php
session_start();
include 'koneksi.php';
$today = date("Ymd");
$tanggal_daftar = date("d/m/Y");
$query_putri = "SELECT max(id_tagihan) as maxID FROM tagihan WHERE id_tagihan LIKE '$today%'";
$hasil = mysqli_query($koneksi, $query1_putri);
$data = mysqli_fetch_array($hasil);
$idMax = $data['maxID'];


$NoUrut = (int) substr($idMax, 8, 3);
$NoUrut++;
$NewID = $today .sprintf('%03s', $NoUrut);


$id_pelanggan =$_SESSION['id_pelanggan'];
$query_penggunaan_putri = mysqli_query($koneksi, "SELECT * FROM penggunaan WHERE id_pelanggan='$id_pelanggan'");
$penggunaan_putri = mysqli_fetch_array($query_penggunaan_putri);
$id_penggunaan = $penggunaan_putri['id_penggunaan'];

$query_pelanggan_putri = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan' ");
$pelanggan_putri = mysqli_fetch_array($query_pelanggan_putri);
$nama_pelanggan_putri = $pelanggan_putri['nama_pelanggan'];
$id_tarif = $pelanggan['id_tarif'];

$query_tarif_putri=mysqli_query($koneksi, "SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
$tarif_putri =mysqli_fetch_array($query_tarif_putri);
$tarifperkwh = $tarif['tarifperkwh'];

$bulan_tagihan = $penggunaan['bulan'];
$tahun_tagihan = $penggunaan['tahun'];


$jumlah_meter_penggunaan = $penggunaan['meter_akhir']-$penggunaan['meter_awal'];
$jumlah_tagihan = $jumlah_meter_penggunaan*$tarifperkwh;


$tahun_cek = date("Y");
$bulan_cek = date("n");

$tagihan_belum_dibayar = mysqli_query($koneksi,"SELECT * FROM tagihan WHERE id_pelanggan='$id_pelanggan' AND bulan='$bulan_cek' AND tahun='$tahun_cek' AND status='Belum Dibayar'");

$cek_tagihan_belum_dibayar = mysqli_num_rows($tagihan_belum_dibayar);
if($cek_tagihan_belum_dibayar > 0)
{
    echo "<script>window.alert('Jangan cek terus - terusan, masih ada tagihan yang belum dibayar hehehe')</script>";
    $tagihan = mysqli_fetch_array($tagihan_belum_dibayar);
    $status=$tagihan['status'];
    $id_tagihan = $tagihan['id_tagihan'];
}else
{
    mysqli_query($koneksi, "INSERT INTO tagihan VALUES ('$NewID','$id_penggunaan','$bulan_tagihan','$tahun_tagihan','$jumlah_meter_penggunaan','Belmu Dibayar')");
    $status= "Belum Dibayar";
    $id_tagihan = $NewID;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Putri PLN</title>
    <link href="style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><span>P P L N</span></a>
                    </div>
                    <div class="clearfix"></div>
                <br />
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a href="informasi.php"><i class="fa fa-user"></i> Informasi Akun </a>
                                </li>
                                <li><a href="saldo.php"><i class="fa fa-dollar"></i> Informasi Saldo </span></a>
                                </li>
                                <li><a href="tagihan.php"><i class="fa fa-desktop"></i> Tagihan </a>
                                </li>
                                <li><a href="riwayatpembelian.php"><i class="fa fa-table"></i> Riwayat Transaksi </a>
                                </li>
                            </ul>
                        </div>
                        </div>
                    <!-- /sidebar menu -->
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                    </nav>
                    <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <div class=" fa fa-list"></div>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="bantuan.php">Help</a>
                                    </li>
                                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                    <li><a href="bahasaIndonesia/informasi.php">Bahasa Indonesia</a>
                                    </li>
                                </ul>
                            </li>
                                <li role="presentation" class="dropdown">
                         </li>
                     </ul>
                </div>
            </div>
            <div class="right_col" role="main">
                <!-- top tiles -->
                <div class="content">
                           <div class="x_panel">
                                <div class="x_title">
                                    <h2>Bill</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form class="form-horizontal form-label-left input_mask" method="POST" action="pembayaran_proses.php?id_tagihan=<?php echo $NewID; ?>">
                                        <div class="form-group">
                                            <label>Nama Pelanggan</label>
                                            <input type="text" name="nama_pelanggan" class="form-control" value="<?=$pelanggan['nama_pelanggan'];?>" >
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor KWH</label>
                                            <input type="text" name="nomor_kwh" class="form-control" value="<?=$pelanggan['nomor_kwh'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Daya WATT</label>
                                            <input type="number" name="daya" class="form-control" value="<?=$tarif['daya'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Tarif Per-KWH</label>
                                            <input type="number" name="tarifperkwh" class="form-control" value="<?=$tarif['tarifperkwh'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Bulan Tagihan</label>
                                            <input type="text" name="bulan_tagihan" class="form-control" value="<?=$penggunaan['bulan'];?> <?=$penggunaan['tahun'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Jumlah Tagihan</label>
                                            <input type="number" name="jumlah_meter" class="form-control" value="<?php echo $jumlah_tagihan; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <input type="text" name="status" class="form-control" value="Belum Dibayar">
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" name="bayar" class="btn btn-success" value="bayar">
                                        </div>
                                      </form>
                                </div>
                            </div>
                          </div>
                      </div>
                <!-- /top tiles -->
                
            </div>
        </div>

    </div>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>

