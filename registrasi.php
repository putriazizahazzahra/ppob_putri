<?php
include 'koneksi.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>PPLN</title>
	<link rel="stylesheet" href="style.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<style type="text/css">
  body{
  font-family: sans-serif;
  background: #f9da3a;
}

h1{
  text-align: center;
  /*ketebalan font*/
  font-weight: 300;
}

.tulisan_login{
  text-align: center;
  /*membuat semua huruf menjadi kapital*/
  text-transform: uppercase;
}

.kotak_login{
  width: 350px;
  background: white;
  /*meletakkan form ke tengah*/
  margin: 80px auto;
  padding: 30px 20px;
  
}

label{
  font-size: 11pt;
}

.form_login{
  /*membuat lebar form penuh*/
  box-sizing : border-box;
  width: 100%;
  padding: 10px;
  font-size: 11pt;
  margin-bottom: 20px;
}

.tombol_login{
  background: #2aa7e2;
  color: white;
  font-size: 11pt;
  width: 100%;
  border: none;
  border-radius: 3px;
  padding: 10px 20px;
}

.link{
  color: #232323;
  text-decoration: none;
  font-size: 10pt;
}

.alert{
  background: #e44e4e;
  color: white;
  padding: 10px;
  text-align: center;
  border:1px solid #b32929;
}
</style>
<body>
 <div class="kotak_login">
    <p class="tulisan_login">LOGIN</p>
 	<form action="regist_proses.php" method="post">
      <div class="form-group">
      <label>Username</label>
      <input type="text" name="username" autocomplete="off" class="form-control" placeholder="Masukan Username" required="required">
	  </div>
	  <div class="form-group">
	  <label>Password</label>
      <input type="password" name="password" autocomplete="off" class="form-control" placeholder="Masukan Password" required="required">
      </div>
      <div class="form-group">
      <label>Nomor KWH</label>
      <input type="number" name="nomor_kwh" autocomplete="off" class="form-control" placeholder="Masukan Nomor KWH" required="">	
      </div>
      <div class="form-group">
      <label>Nama Lengkap</label>
      <input type="text" name="nama_pelanggan" autocomplete="off" class="form-control" placeholder="Masukan Nama Lengkap" required="">
      </div>
      <div class="form-group">
      <label>Alamat</label>
      <input type="text" name="alamat" autocomplete="off" class="form-control" placeholder="Masukan Alamat" required="">
      </div>
      <div class="form-group">
      <label>Tarif</label>
      <select class="form-control" name="id_tarif">
      	<?php
      	include 'koneksi.php';
      	$query_tarif_putri = mysqli_query($koneksi, "SELECT * FROM tarif ");
      	while($tarif_putri=mysqli_fetch_array($query_tarif_putri))
      	{
      	?>
      	<option value="<?=$tarif_putri['id_tarif'];?>"><?=$tarif_putri['daya'],"watt - ",$tarif_putri['tarifperkwh']," Per KWH";?></option>
      	<?php
      }
      ?>
      </select>
      </div>
      <input type="submit" class="tombol_login" name="simpan" value="DAFTAR">
      <br/>
      <br/>     
    </form>
    
  </div>
</body>
</html>