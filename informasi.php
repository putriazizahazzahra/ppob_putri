 <?php
               session_start();
               include 'koneksi.php';
               $id_pelanggan =$_SESSION['id_pelanggan'];
               $query_pelanggan_putri = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan'");
               $pelanggan = mysqli_fetch_array($query_pelanggan_putri);
               
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Putri PLN</title>
    <link href="style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><span>P P L N</span></a>
                    </div>
                    <div class="clearfix"></div>
                <br />
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a href="informasi.php"><i class="fa fa-user"></i>Informasi Akun</a>
                                </li>
                                <li><a href="saldo.php"><i class="fa fa-dollar"></i>Informasi Saldo</span></a>
                                </li>
                                <li><a href="tagihan.php"><i class="fa fa-desktop"></i>Tagihan</a>
                                </li>
                                <li><a href="riwayatpembelian.php"><i class="fa fa-table"></i>Riwayat Transaksi</a>
                                </li>
                            </ul>
                        </div>
                        </div>
                    <!-- /sidebar menu -->
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                    </nav>
                    <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <div class=" fa fa-list"></div>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="bantuan.php">Bantuan</a>
                                    </li>
                                    <li><a href="logout.php">Log Out</a>
                                    </li>
                                </ul>
                            </li>
                                <li role="presentation" class="dropdown">
                         </li>
                     </ul>
                </div>
            </div>

            <div class="right_col" role="main">

                <!-- top tiles -->
               
                <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Profil</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <div class="table-responsive">
                                  <table class="table">
                                    <tr>
                                        <td>ID Pengguna</td>
                                        <td><?=$pelanggan['id_pelanggan'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Nomor KWH</td>
                                        <td><?=$pelanggan['nomor_kwh'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Pelanggan </td>
                                        <td><?=$pelanggan['nama_pelanggan'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td><?=$pelanggan['alamat'];?></td>
                                    </tr>
                                    </table>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                <!-- /top tiles -->
                
            </div>
        </div>

    </div>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>
