<!DOCTYPE html>
<html lang="en">
<head>
    <title>Putri PLN</title>
    <link href="style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><span>P P L N</span></a>
                    </div>
                    <div class="clearfix"></div>
                <br />
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a href="informasi.php"><i class="fa fa-user"></i> Informasi Akun </a>
                                </li>
                                <li><a href="saldo.php"><i class="fa fa-dollar"></i> Informasi Saldo </span></a>
                                </li>
                                <li><a href="tagihan.php"><i class="fa fa-desktop"></i> Tagihan </a>
                                </li>
                                <li><a href="riwayatpembelian.php"><i class="fa fa-table"></i> Riwayat Pembelian </a>
                                </li>
                            </ul>
                        </div>
                        </div>
                    <!-- /sidebar menu -->
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                    </nav>
                    <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <div class=" fa fa-list"></div>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="bantuan.php">Help</a>
                                    </li>
                                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                                <li role="presentation" class="dropdown">
                         </li>
                     </ul>
                </div>
            </div>
            <div class="right_col" role="main">
                <!-- top tiles -->
                <div class="content">
                        <div class="col-md-12" >
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Penggunaan</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form class="form-horizontal form-label-left input_mask">

                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left" id="inputSuccess2" name="id_pelanggan" placeholder="ID Pelanggan" readonly="">
                                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left" id="inputSuccess2" name="bulan_pembayaran" required="" autocomplete="off" placeholder="Bulan Pembayaran">
                                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left" id="inputSuccess4" required="" name="tanggal_pembayaran" autocomplete="off" placeholder="Tanggal Pembayaran">
                                            <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left" id="inputSuccess2" name="tagihan" required="" autocomplete="" placeholder="Tagihan">
                                            <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left" required="" name="biaya_admin" autocomplete="off" id="inputSuccess2" placeholder="Biaya Admin Bank">
                                            <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left" id="inputSuccess2" name="total_bayar" placeholder="Total Bayar" readonly="">
                                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-10">
                                               <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Cetak Struk</button>
                                               <div id="myModal" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">
                                                     <!-- konten modal-->
                                                     <div class="modal-content">
                                                    <!-- heading modal -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Bagian heading modal</h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body">
                                                        <p>bagian body modal.</p>
                                                    </div>
                                                    <!-- footer modal -->
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-success" data-dismiss="modal">Tutup Modal</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                          </div>
                      </div>
                <!-- /top tiles -->
                
            </div>
        </div>

    </div>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>
