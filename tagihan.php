<?php
session_start();
               include 'koneksi.php';
               $id_pelanggan =$_SESSION['id_pelanggan'];
               $query_pelanggan_putri = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan'");
               $pelanggan = mysqli_fetch_array($query_pelanggan_putri);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Putri PLN</title>
    <link href="style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><span>P P L N</span></a>
                    </div>
                    <div class="clearfix"></div>
                <br />
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a href="informasi.php"><i class="fa fa-user"></i>Informasi Akun</a>
                                </li>
                                <li><a href="saldo.php"><i class="fa fa-dollar"></i>Informasi Saldo</span></a>
                                </li>
                                <li><a href="tagihan.php"><i class="fa fa-desktop"></i>Tagihan</a>
                                </li>
                                <li><a href="riwayatpembelian.php"><i class="fa fa-table"></i>Riwayat Transaksi</a>
                                </li>
                            </ul>
                        </div>
                        </div>
                    <!-- /sidebar menu -->
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                    </nav>
                    <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <div class=" fa fa-list"></div>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="bantuan.php">Help</a>
                                    </li>
                                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                                <li role="presentation" class="dropdown">
                         </li>
                     </ul>
                </div>
            </div>
            <div class="right_col" role="main">
                <!-- top tiles -->
                <div class="content">
                           <div class="x_panel">
                                <div class="x_title">
                                    <h2>Bill</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form method="POST" action="cek_tagihan_proses.php">
                                            <div class="col-md-6 ">
                                                <label>Masukan Nomor KWH</label>
                                                <input type="text" name="meter_akhir" value="<?=$pelanggan['nomor_kwh']?>" class="form-control" autocomplete="off" placeholder="Enter Your Last Metre">
                                            </div>
                                            <div class="col-md-6 ">
                                                <label>Masukan Meter Akhir</label>
                                                <input type="text" name="meter_akhir" class="form-control" autocomplete="off" placeholder="Enter Your Last Metre">
                                            </div>
                                                <button type="submit" name="cek_tagihan" class="btn btn-success">Submit</button>
                                        </div>
                                        </form>
                                      </form>
                                </div>
                            </div>
                          </div>
                      </div>
                <!-- /top tiles -->
            </div>
        </div>

    </div>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>
